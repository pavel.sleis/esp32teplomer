#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include "wificonf.h"

#define uS_TO_S_FACTOR 1000000  // prevod mikrosekund na sekundy
#define TIME_TO_SLEEP 360       // pocet sekund v hlubokem v DeepSleep
#define ONE_WIRE_PIN 12         // pin pro cteni data teplomeru MISO
#define GPIO_OUT_TEMP 14        // pin pro napajeni teplomeru SCL
#define GPIO_INDICATOR 2        // indikator odesilani dat (modra LED na desce)
#define analogInPin A0
#define R_top 320               // hodnota horniho odporu delice pro mereni napeti baterky (320 k pro FireBeetle desku)
#define R_bot 100               // hodnota spodniho odporu delice pro mereni napeti baterky (320 k pro FireBeetle desku)
#define ADC_Umax 1.00F          // referencni napeti AD prevodniku (1,00 V pro ESP8266)
#define ADC_Nmax 1023           // rozliseni AD prevodniku (10b pro ESP8266)
#define ADC_V_corr 0.05F        // korekce nepresnosti ADC, pricita se k vysledku [V]

#define MAX_ATTEMPT_CONN 5      // max attempts(+1) pocet pokusu pripojot se k wifi v jednom cyklu
#define DEBUG false              // seriove vypisy jsou zapnuty pouze pri DEBUG true

// include from wificonf.h
const char* SSID     = MACH_WIFI_SSID;
const char* PASSWORD = MACH_WIFI_PSWD;
const char* DEVICE_NAME = DEVNAME;

String SERVER_URL = SERVER;

int CONN_DELAY = 1000;          // defaultni delay pro opakovane pokusy pripojit se k WiFi

long RUN_TIME=0;                // merim delku behu script, a tu odectu od deep sleep 

OneWire oneWire(ONE_WIRE_PIN);
DallasTemperature tempSensor(&oneWire);

float get_temperature(){
  tempSensor.begin();
  tempSensor.setWaitForConversion(true);
  tempSensor.requestTemperatures();
  return tempSensor.getTempCByIndex(0);
}

float get_voltage(){
  return ADC_Umax*(float)(analogRead(analogInPin))/ADC_Nmax*(1.0F+R_top/R_bot) + ADC_V_corr;
}

void send_data_http(String URL){
  HTTPClient http;

  // adresa serveru
  http.begin(URL);

  if (DEBUG) Serial.print("Spojeni se serverem GET...\n");
  // navaz spojeni metodou GET
  int httpCode = http.GET();

  // httpCode will be negative on error
  if (DEBUG) {
    if(httpCode <= 0) {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    } else {
      // HTTP header byla odeslana a server odpovedel  httpCode
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      // file found at server
      if(httpCode == HTTP_CODE_OK) {
        //String payload = http.getString();
        //Serial.println(payload);
        Serial.printf("%s\n", http.getString().c_str());
        Serial.println("[HTTP] END");
      }
    }
  }
  http.end();
}

void setup() {
  RUN_TIME =micros();
  WiFi.disconnect(true);
  WiFi.begin(SSID, PASSWORD);
  delay(CONN_DELAY);  //bez tohoto cekani se nechce spojovat wifi na FireBeetle ESP8266
  
  if (DEBUG) {
    Serial.begin(115200);
    Serial.print("\n ### Zaciname ###");
  }

  if (DEBUG) Serial.print("\nConnecting to: " + String(SSID));

// concounter je počítadlo pro MAX_ATTEMPT_CONN
  int concounter = 0;
  while((WiFi.status() != WL_CONNECTED) && (concounter < MAX_ATTEMPT_CONN)) {
    if (DEBUG) Serial.print(".");
    concounter++;
    delay(CONN_DELAY);
  }

  if (DEBUG) Serial.print(" .o0O - Connected\n");

  // odesli data pokud je wifi spojena
  if(WiFi.status() == WL_CONNECTED) {
    // zapni napajeni teplomeru
    pinMode(GPIO_OUT_TEMP, OUTPUT);
    digitalWrite(GPIO_OUT_TEMP, HIGH);
    // rozsvit indikaci
    pinMode(GPIO_INDICATOR, OUTPUT);
    digitalWrite(GPIO_INDICATOR, HIGH);
    if (DEBUG) Serial.print("Get MAC address, temperature, voltage and build the URL string\n");
    SERVER_URL = SERVER_URL + "?mac=" + WiFi.macAddress() + "&name=" + DEVICE_NAME + "&tempV=" + get_temperature() + "&voltageV=" + get_voltage() + "&signalstrengthV=" +WiFi.RSSI();
    if (DEBUG){
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      Serial.print("GW address: ");
      Serial.println(WiFi.gatewayIP());
      Serial.print("WiFi signal strength: ");
      Serial.println(WiFi.RSSI());
      Serial.println("Data URL: " + SERVER_URL);
      Serial.print("Send data to server\n");
    }
    // zhasni indikaci provozu
    digitalWrite(GPIO_INDICATOR, LOW);
    // vypni napajeni teplomeru
    digitalWrite(GPIO_OUT_TEMP, LOW);

    send_data_http(SERVER_URL);
  } else if (DEBUG) {
    Serial.println("\nNot connected to WiFi");
  }

  // a hajdy na kute
  if (DEBUG){
    Serial.println("Setup ESP6288 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");
    Serial.println("Going to sleep now");
    //Serial.flush(); 
  }
  RUN_TIME = (TIME_TO_SLEEP * uS_TO_S_FACTOR)-(micros()-RUN_TIME);
  //if ( RUN_TIME > 0){
    ESP.deepSleep(RUN_TIME);
  //} else {
  //  ESP.deepSleep(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  //}
}

void loop(){
}
